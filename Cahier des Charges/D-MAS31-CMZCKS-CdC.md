---
title: MAS 3.1 - Cahier des Charges
author: Cédric Martinez, Cédric Kreis et Cédric Tenthorey
geometry: margin=2cm,a4paper
fontsize: 12pt
fontfamily: avant
lang: french
toccolor: black
numbersections: yes
header-includes:
- \usepackage{fancyhdr}
- \usepackage{xcolor}
- \usepackage{hyperref}
- \usepackage{titling}
- \usepackage[T1]{fontenc}
- \usepackage{lmodern}
- \usepackage{lastpage}
- \usepackage{ragged2e}
- \usepackage{lscape}
- \newcommand{\blandscape}{\begin{landscape}}
- \newcommand{\elandscape}{\end{landscape}}
- \hypersetup{colorlinks = true,linkcolor = black}
- \pagestyle{fancy}
- \fancyhead[LO,LE]{CPNV}
- \fancyfoot[C]{Page \char58 \ \thepage \ sur \pageref{LastPage}}
- \fancyfoot[RO,LE]{Créé le \char58 \  \today}
- \fancyfoot[RE,LO]{Auteurs \char58 \ CMZ, CKS et CTY}
link-citations: true
nocite: |
  @*
---
\centering
![Logo](..\Figures\fosscloud.jpg)
\newpage

\raggedright
\tableofcontents

\newpage
\justify

# Introduction

Notre groupe, qui est composé de 3 techniciens, est mandaté par une entreprise afin de mettre en place une infrastructure et d'implémenter une solution Desktop-as-a-Service de notre choix dans celle-ci. Par conséquent, nous allons en premier lieu analyser les besoins de l'entreprise afin de choisir la solution qui répond au mieux à leurs besoins. De plus, la rédaction de ce cahier des charges, ainsi que de l'offre au client (qui sera rédigé plus tard dans ce projet) permettra à l'entreprise d'avoir une vue précise des tâches qui seront effectuées lors de ce projet. Cela va également permettre de déterminer les objectifs principaux et secondaires du projet et finalement de définir les contraintes à respecter.

## Personnes responsables du projet

| Prénom | Nom | Fonction | adresse email |
| :------------- | :------------- | :------------- | :------------- |
| Cédric | Kreis | Technicien ES | cedric.kreis@cpnv.ch |
| Cédric | Martinez | Technicien ES | cedric.martinez@cpnv.ch |
| Cédric | Tenthorey | Technicien ES | cedric.tenthorey@cpnv.ch |

## Desktop as a Service

Le mot DaaS est l'acronyme de "Desktop as a service", il s'agit de fournir une infrastructure de bureaux virtuels à un client à la demande. Le but est de fournir une solution qui permette de dématérialiser l'environnement de travail pour l'utilisateur et faire qu'il puisse accéder à son environnement de travail depuis n'importe quel terminal.
La solution remet au gout du jour le lien serveur-terminal qui avait été remplacé par les PCs qui permettaient d'accéder à l'environnement de travail en local avec les contraintes qui en découlent.
Cette solution permet d'économiser sur les ressources matérielles puisqu'au niveau du client n'importe quel terminal peut être utilisé pour autant qu'il dispose d'un navigateur web et d'une connexion à Internet. Dans le cas présent, le parc informatique vieillissant de l'entreprise n'a plus besoin d'être remplacé car les ressources matérielles seront exploitées depuis les hyperviseurs hébergeant la solution de DaaS. L'avantage principal pour l'entreprise est de ne pas avoir à investir dans l'infrastructure nécessaire au bon fonctionnement du DaaS car elle utilise les ressources du fournisseur. Ce qui permet une réduction du coût du secteur informatique, une simplification de la migration des postes utilisateurs et de leurs données.
Par contre, le DaaS implique une dépendance à la connexion Internet, en effet, les utilisateurs ne peuvent accéder aux environnements de travail sans connexion internet. La possibilité de synchroniser les données des utilisateurs en cas de coupure de connexion à Internet est possible mais cette fonctionnalité est peu rependue. Sans oublier, le DaaS contribue à la protection de l'environnement puisque la diminution du nombre d'équipements informatiques ainsi que la mutualisation des environnements de travail permettent de diminuer la consommation d'électricité et d'eau utilisé pour le refroidissement.

## Objectifs

L'objectif principal de ce projet est d'implémenter une solution DaaS qui doit être accessible depuis internet pour 30 utilisateurs. Concernant les objectifs secondaires, nous devons être en mesure de mettre en place la solution choisie dans le temps imparti. Finalement, nous allons mettre en place plusieurs protocoles de tests afin de garantir au client le bon fonctionnement des différents points que nous aurons définis avec lui.  

# Contraintes

Vous trouverez ci-dessous les différentes contraintes qui ont été fixées avec le client:

- La solution doit permettre d’héberger 30 utilisateurs simultanément
- L'accès externe WAN (internet) doit s'effectuer sur un seul point d’entrée.
- Tout le trafic réseau doit passer uniquement par le port 443 en HTTPS
- La solution de VDI choisie doit permettre de déployer des clients virtuels Windows 7 (Ubuntu en bonus)
- Les machines clientes virtuelles doivent être accessibles par des ordinateurs ayant un des trois système d’exploitation suivant : OSX, Linux ou Windows
- Les ordinateurs clients de la société ne sont pas dans le domaine
- La connexion doit être automatique au démarrage de la machine cliente.
- Les utilisateurs de la société doivent être centralisés dans un seul système

Des tests seront effectués à l'aide d'un logiciel de charge afin de simuler la connexion simultanée de 30 utilisateur pour valider la solution.
L'accès depuis le WAN suppose la mise en place de règle sur le pare-feu des hyperviseurs et de systèmes de détection de DDoS comme fail2ban.
Un script devra être mis en place afin de lancer automatiquement le navigateur web de la machine client avec l'URL d'accès aux machines virtuelles.
Un système centralisé d'authentification nécessite un domaine LDAP ou Active Directory.

\newpage

# Analyse des solutions

Comme expliqué plus haut dans ce document, nous avons effectué une analyse des différents produits sur le marché afin d'en choisir un qui répond au mieux aux besoins de notre client. Vous trouverez ci-dessous un tableau comparatif de 3 solutions qui offrent la possibilité d'effectuer du DaaS.

## Comparatif des solutions

| Fonctionnalité | FOSS-Cloud | VMware Horizon Cloud | Citrix Cloud |
| :------------- | :------------- | :------------- | :------------- |
| Prix | Gratuit | 842 USD / mois | 845 USD / mois |
| Licence | EUPL | Propriétaire | Propriétaire |
| Nombre d'utilisateurs | 30 | 50 | 30 |
| OS invité | Windows/Linux | Windows/Linux | Windows/Linux |
| OS Client | Windows/OSX/Linux | Windows/OSX/Linux | Windows/OSX/Linux |
| Accès aux VMs | SPICE Client | HTML5 | HTML5 |
| Port utilisé | 443, 5900 | 443 | 443 |
| Domaine | LDAP | Active Directory | LDAP |

Comme nous pouvons le constater dans le tableau ci-dessus, les trois solutions offrent la possibilité aux clients qui utilisent les systèmes d'exploitation Windows, GNU/Linux, ou Mac OSX de se connecter aux machines virtuelles mises à disposition. De plus, les trois produits offrent également la possibilité de virtualiser et de déployer des machines virtuelles ayant le système d'exploitation Windows 7 comme demandé par le client. Les solutions permettent également à 30 utilisateurs de se connecter simultanément. Le justificatif de notre choix ainsi que la description de la solution choisie est détaillé dans le paragraphe suivant.

\newpage

## Solution choisie

Après analyse, nous avons décidé de choisir la solution FOSS-Cloud car elle offre un avantage conséquent sur le point des licences. En effet, cette solution est gratuite et il n'y a, de ce fait, pas de frais de licence, étant donné que ce sont des licences EUPL. A l'inverse de ses concurrents, pour lesquelles, un abonnement (annuel ou mensuel) doit être contracté. Par conséquent, cela va permettre à la société de limiter les coûts dans ce projet. L'argent économisé pourra être utilisé ailleurs dans la société, comme par exemple pour la maintenance et l'entretien du parc informatique. A l'inverse de ses concurrents, FOSS-Cloud n'utilise pas HTML5 pour l'accès aux VM mais le client "SPICE Client".

FOSS-Cloud est un système d'exploitation faisant office d'hyperviseur basé sur kvm dont le but est de fournir une infrastructure de virtualisation et de services dans le cloud de manière redondante. Les fonctionnalités principales de cette solution sont la capacité de fournir une infrastructure VDI et VSI (Virtual Server Infrastructure) et de fournir des bureaux selon un modèle de machine virtuelle à la demande selon des quotas. Cette solution permet d'utiliser plusieurs serveurs nommés nodes afin d'assurer la redondance et la scalabilité face à la montée en charge. Il est possible de créer des utilisateurs et des groupes qui sont associés à des profils de machines virtuelles qui sont accessibles à l'aide du client virt-viewer (anciennement SPICE Client). Le but étant de permettre à un utilisateur d'accéder à l'environnement de travail en se connectant sur l'interface de gestion de FOSS-Cloud et en cliquant sur sa machine virtuelle assignée automatiquement selon son groupe. L'utilisateur peut alors utiliser des applications utilisant du multimédia ou des périphériques USB comme sur un poste de travail standard.

\newpage

# Prérequis

## Prérequis matériel

Pour ce projet nous avons besoin de quatre serveurs pour monter une architecture Multi-Node. Or cette architecture sera répartie sur deux serveurs physique qui hébergeront chacun deux machines virtuelle. Il y aura de plus un switch qui interconnectera les deux serveurs afin de compléter l'architecture nécessaire pour ce projet.

Vous trouverez ci-dessous la liste des prérequis minimum par noeud que doit posséder un serveur

- Processeur Intel 64-Bit avec VT-X
- 4 Go de mémoire vive
- 320 Go d'espace disque dur
- 2 ports Ethernet par serveur
- 1 switches au gigabit

Dans le cadre de ce projet le matériel est fourni par l'école. De plus, il a été défini que nous partirons du principe que le client possède déjà le matériel nécessaire à la réalisation du projet. De ce fait, la partie hard

## Infrastructure prévue

Pour mettre en place l'infrastructure du projet, deux serveurs physiques Dell Poweredge R430 seront utilisé sur lesquels sera installé l'hyperviseur VMware ESXi 6.5. Sur chacun d'entre eux, deux machines virtuelles avec le système d'exploitation FOSS-Cloud seront mises en place servant de nodes.

Un serveur HP Microserver gen8 sera utilisé afin de servir de SAN via iSCSI entre les deux hyperviseurs ESXi, le système d'exploitation Debian 9 sera installé sur celui-ci, le stockage sera configuré avec un RAID5 afin d'assurer la redondance des données.

Chaque serveur Dell Poweredge R430 dispose d'un processeur Intel avec 8 coeurs, de 32 Gb de RAM et d'un disque dur de 500 GB. Ces spécifications permettent de faire fonctionner l'infrastructure au complet sur un seul hyperviseur en cas de crash de l'autre hyperviseur ESXi.

\newpage

# Spécifications techniques

## VLANs

L'architecture requiert de plus 4 VLANs afin de pouvoir fonctionner correctement.

Le tableau ci-dessous permet de visualiser les différents VLANs qui seront utilisés afin de mener à bien ce projet.

| Description du VLAN | VLAN ID | Subnet |
| :------------- | :------------- | :------------- |
| Pub | 1 | 172.17.0.0/16 |
| Admin | 100 | 192.168.1.0/24 |
| Int | 200 | 192.168.2.0/24 |
| Data | 300 | 192.168.3.0/24 |
| Storage | 400 | 192.168.4.0/24 |

- Le VLAN Pub sera utilisé afin d'accéder à l'interface de gestion des machines virtuelles via virt-viewer et à Internet.
- Le VLAN Admin sera utilisé sur les nodes afin de monitorer le réseau.
 -Le VLAN Data sera utilisé afin que les nodes GLusterFS puissent communiquer entre eux.
- Le VLAN Int sera utilisé afin que le trafic interne libvirt puisse se faire les deux nodes VM.
- Le VLAN Storage sera utilisé afin que le SAN et les deux hyperviseurs ESXi puissent communiquer via iSCSI.

Pour de plus amples informations, se référer au schéma réseau situé dans les annexes.

\newpage  

# Annexes

## Schéma réseau

Vous trouverez ci-dessous le schéma réseau du projet.

![Schéma réseau](..\Figures\I-SAAS-CMZ-NetworkScheme.png)

\newpage
\blandscape

## Planning du projet

![Planning du projet](..\Figures\planning_cdc.png)

\elandscape
