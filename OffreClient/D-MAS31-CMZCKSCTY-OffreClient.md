---
title: MAS 3.1 - Offre client
author: Cédric Martinez, Cédric Kreis, Cédric Tenthorey
geometry: margin=2cm,a4paper
fontsize: 12pt
fontfamily: avant
lang: french
toccolor: black
numbersections: yes
header-includes:
- \usepackage{fancyhdr}
- \usepackage{xcolor}
- \usepackage{hyperref}
- \usepackage{titling}
- \usepackage[T1]{fontenc}
- \usepackage{lmodern}
- \usepackage{lastpage}
- \usepackage{ragged2e}
- \usepackage{lscape}
- \newcommand{\blandscape}{\begin{landscape}}
- \newcommand{\elandscape}{\end{landscape}}
- \hypersetup{colorlinks = true,linkcolor = black}
- \pagestyle{fancy}
- \fancyhead[LO,LE]{CPNV}
- \fancyfoot[C]{Page \char58 \ \thepage \ sur \pageref{LastPage}}
- \fancyfoot[RO,LE]{Créé le \char58 \  \today}
- \fancyfoot[RE,LO]{Auteurs \char58 \ CMZ, CKS, CTY}
link-citations: true
nocite: |
  @*
---
\centering
![Logo](..\Figures\fosscloud.jpg)
\newpage

\raggedright
\tableofcontents

\newpage
\justify

# Spécifications

## Introduction

Le groupe, qui est composé de 3 techniciens, est mandaté par une entreprise afin de mettre en place une infrastructure et d’implémenter la solution FossCloud. Le but de ce projet est de fournir à l'entreprise une infrastructure de bureaux virtuels afin que chacun de leurs employés puissent accéder à leur environnement de travail depuis n'importe quel terminal. Dans le cas présent, le parc informatique vieillissant de l’entreprise n’a plus besoin d’être remplacé car les ressources matérielles seront exploitées depuis les hyperviseurs hébergeant la solution de DaaS.

## Prérequis

Afin de mener à bien ce projet, il est nécessaire d'avoir un certain nombre de prérequis. Ceux-ci sont énumérés dans cette rubrique.
Le matériel hardware afin de réaliser ce projet est déjà en possession du client. Ce matériel comprend :

- 1 ProLiant Micro server Gen8
- 2 Dell Poweredge R420
- 1 Switch gigabit

## Prérequis hardware FossCloud

Les prérequis ci-dessous sont nécessaires pour chaque serveur FossCloud :

- Processeur Intel 64-Bit avec VT-X
- 4 Go de RAM
- 320 Go HDD
- 2 ports Ethernet

De plus, le client va héberger ses serveurs dans ses propres locaux, ce qui lui évite des frais supplémentaires en devant louer un rack dans un datacenter.

\newpage

## Objectifs

### Principaux

* La solution doit permettre à 30 utilisateurs de travailler simultanément
* Tout le trafic réseau doit passer par le port 443 (HTTPS)
* Mise à dispostion de VM Windows 10 aux clients
* L’accès externe WAN (internet) doit s’effectuer sur un seul point d’entrée
* Accessibilité des VM depuis les OS client ci-dessous :
    * Windows
    * Linux
    * OSX
* Les machines clients ne sont pas dans le domaine
* Centralisation des utilisateurs sur un seul système
* Connexion automatique au démarrage de la machine client

### Secondaires

- Mise a disposition de VM clients avec les OS ci-dessous
    - Windows 7
    - Ubuntu
- Mise en place de la solution dans le temps imparti
- Mise en place d'un protocole de test
- Mise en place de la haute disponibilité

\newpage

## Planification initiale

Une planification initiale a été effectuée dans le cahier des charges et est également disponible plus bas dans ce document.


## Livrables

### Cahier des charges

Un cahier des charges a été rédigé et transmis au client le 1 décembre 2017. Il se trouve en annexe de cette offre.

### Offre au client

Ce document contient une proposition commerciale décrivant l'ensemble des produits, services et coûts du projet. L’offre sera rendue au plus tard le 18 décembre 2018 au client.


### Protocole de tests

Confirme le bon fonctionnement des points clés du projet et discerne les éventuels comportements problématiques.

### Planning GANTT

Vous trouverez le planning initial du projet en annexes et le récapitulatif des dates de rendus ci-dessous :

- Début du projet : 13 novembre 2017
- Remise du cahier des cahrges : 1er décembre 2017
- Remise de l'offre au client : 18 décembre 2017
- Rendu du projet : 23 mars 2018


\newpage

# Analyse

## Cahier des charges détaillé

### Mise en place de l'infrastructure informatique

#### FOSSCloud

\mbox{}

L’hyperviseur VMware ESXi 6.5 sera installé sur chacun des deux serveurs physiques Dell Poweredge R420. De plus, une VM FossCloud Node et une VM FossCloud storage seront mises en place sur chacun des hyperviseurs.
Afin que les deux serveurs ESXI soient en cluster, un serveur HP Microserver gen8 sera utilisé afin de servir de SAN (connexion en ISCSI). Le système d’exploitation Debian 9 sera installé sur le HP Microserver. De plus, un RAID1 logiciel sera configuré afin d’assurer la redondance des données.   

####  VLAN

\mbox{}

L'architecture réseau sera composée de 5 VLANs

- Un VLAN **Pub** sera utilisé afin d’accéder à l’interface de gestion des machines virtuelles via virt-viewer et à Internet.
- Un VLAN **Admin** sera utilisé sur les nodes afin de monitorer le réseau.
- Un VLAN **Data** sera utilisé afin que les nodes GLusterFS puissent communiquer entre eux.
- Un VLAN **Int** sera utilisé afin que le trafic interne libvirt puisse se faire les deux nodes VM.
- Un VLAN **Storage** sera utilisé afin que le SAN et les deux hyperviseurs ESXi puissent communiquer via iSCSI.

Pour plus de précision, vous trouverez le schéma réseau dans le point suivant.

#### Mise en place des VM

\mbox{}

La création d'un template Windows 10 sera effectuée sur les nodes FossCloud, suivi de la mise en place de machines virtuelles dynamiques pour chaque utilisateur en fonction de leur appartenance à un groupe.

\newpage

#### Schéma réseau

\mbox{}

![Schéma réseau](..\Figures\I-SAAS-CMZ-NetworkScheme.png)

\newpage

### Objectifs secondaires

#### Protocole de test

\mbox{}

Un protocole de test certifiant le bon fonctionnement de chaque étape de la mise en oeuvre globale sera effectué.

#### Haute disponibilité

\mbox{}

Si le temps le permet, la mise en place d'une redondance des équipements permettant la haute disponibilité de l'infrastructure FOSS-Cloud sera effectuée.

\newpage

## Budget

Le budget de ce projet a été calculé et est disponible ci-dessous. A noter que les frais de licences des OS utilisateurs sont à la charge de l'entreprise.

![Budget](..\Figures\budget.png)

\newpage

# Annexes

## Cahier des charges

Vous trouverez ci-dessous le cahier des charges.

![Cahier des charges page 1](..\Figures\D-MAS31-CMZCKS-CdC-01.png)

![Cahier des charges page 2](..\Figures\D-MAS31-CMZCKS-CdC-02.png)

![Cahier des charges page 3](..\Figures\D-MAS31-CMZCKS-CdC-03.png)

![Cahier des charges page 4](..\Figures\D-MAS31-CMZCKS-CdC-04.png)

![Cahier des charges page 5](..\Figures\D-MAS31-CMZCKS-CdC-05.png)

![Cahier des charges page 6](..\Figures\D-MAS31-CMZCKS-CdC-06.png)

![Cahier des charges page 7](..\Figures\D-MAS31-CMZCKS-CdC-07.png)

![Cahier des charges page 8](..\Figures\D-MAS31-CMZCKS-CdC-08.png)

![Cahier des charges page 9](..\Figures\D-MAS31-CMZCKS-CdC-09.png)

![Cahier des charges page 10](..\Figures\D-MAS31-CMZCKS-CdC-10.png)

\newpage
\blandscape

## Planning

Vous trouverez ci-dessous le planning du projet.

![Planning du projet](..\Figures\planning_cdc.PNG)

\elandscape
