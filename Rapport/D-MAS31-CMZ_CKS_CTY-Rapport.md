---
title: MAS 3.2 - Rapport
author: Cédric Martinez, Cédric Kreis, Cédric Tenthorey
geometry: margin=2cm,a4paper
fontsize: 12pt
fontfamily: avant
lang: french
toccolor: black
numbersections: yes
header-includes:
- \usepackage{fancyhdr}
- \usepackage{xcolor}
- \usepackage{hyperref}
- \usepackage{titling}
- \usepackage[T1]{fontenc}
- \usepackage{lmodern}
- \usepackage{lastpage}
- \usepackage{ragged2e}
- \hypersetup{colorlinks = true,linkcolor = black}
- \pagestyle{fancy}
- \fancyhead[LO,LE]{CPNV}
- \fancyfoot[C]{Page \char58 \ \thepage \ sur \pageref{LastPage}}
- \fancyfoot[RO,LE]{Créé le \char58 \  \today}
- \fancyfoot[RE,LO]{Auteurs \char58 \ CMZ, CKS, CTY}
link-citations: true
nocite: |
  @*
---
\centering
![Logo](..\Figures\fosscloud.jpg)
\newpage

\raggedright
\tableofcontents

\newpage
\justify

# Introduction

Ce projet a pour but d'implémenter un service de bureau à distance. Les clients pourront s'y connecter via un navigateur web depuis n'importe quel système d'exploitation.

| Fonction | Prénom et nom | Addresse e-mail |
| :------: | :------------- | :------------- |
| Client | Cédric Roten | cedric.roten@cpnv.ch |
| Implémentateur | Cédric Kreis | cedric.kreis@cpnv.ch |
| Implémentateur | Cédric Martinez | cedric.martinez@cpnv.ch |
| Implémentateur | Cédric Tenthorey | cedric.tenthrey@cpnv.ch |

\newpage

# Analyse

## Comparaison des solutions

| Fonctionnalité | Vmware Horizon | XenDesktop | FOSS-Cloud |
| :------------- | :------------- | :------------- | :------------- |
| License | Item Two       | Header Two     | Header Two     |
| Prix | Item Two       | Header Two     | Header Two     |
| Gestion des utilisateurs | Item Two       | Header Two     | Header Two     |

## Solution choisie

La solution choisie pour ce projet est la solution FOSS-Cloud.

\newpage

# Implémentation

## Mise en place de l'infrastructure

L'infrastructure a tout d'abord été mise en place à l'aide d'hyperviseurs mais cette approche a rencontré quelques incidents notammant au niveau des VLANs. En effet, les machines virtuelles hebergeant les noeuds FOSS-Cloud ne pouvaient communiquer entre elles que via des VLANs taggés ce qui n'était pas le cas du VLAN pub.

C'est pour cette raison, qu'il a été décidé d'implémenter l'infrastructure de manière physique sur quatre Dell PowerEdge R430 en y installant sur chacun le système d'exploitation FOSS-Cloud puis en les spécialisant par la suite en noeud de stockage ou noeud de VMs.

L'architecture réseau est composée de 4 VLANs

- Un VLAN **Pub** sera utilisé afin d’accéder à l’interface de gestion des machines virtuelles via virt-viewer et à Internet.
- Un VLAN **Admin** sera utilisé sur les nodes afin de monitorer le réseau.
- Un VLAN **Data** sera utilisé afin que les nodes GLusterFS puissent communiquer entre eux.
- Un VLAN **Int** sera utilisé afin que le trafic interne libvirt puisse se faire les deux nodes VM.

## Configuration du switch

Un switch Cisco 2960 sera utilisé afin d'implémenter le projet.

Pour ce faire, il faut tout d'abord créer les 4 VLANs comme expliqué précédement,

```cisco
conf t

int vlan100
enc dot1q 100
ip address 192.168.1.254 255.255.255.0
no shut

int vlan200
enc dot1q 200
ip address 192.168.2.254 255.255.255.0
no shut

int vlan300
enc dot1q 300
ip address 192.168.3.254 255.255.255.0
no shut

int vlan400
enc dot1q 400
ip address 192.168.4.254 255.255.255.0
no shut

end
wr mem
```l

Ensuite, il faut créer un trunk sur les 8 ports sur lesquels seront connectés les serveurs R420 afin que les VLANs puisse communiquer entre eux.

```cisco
conf t
int range fa0/19-24
switchport mode trunk
switchport trunk allowed vlan 100,200,300,400
no shut

end
wr mem
```

\newpage

# Tests

## Tableau de tests

Vous trouverez ci-dessous le tableau des tests effectués durant le projet.

| Tests effectués | Résultat |
| :-----------: | :----: |
| Ping entre les VLANs | True |
| Ping des serveurs depuis le LAN | True |
| Connexion depuis le LAN  | True |
| Connexion depuis le WAN | True |
| Accès aux bureaux depuis l'interface web | True |
| Item One | Item Two     |
| Item One | Item Two     |
| Item One | Item Two     |
| Item One | Item Two     |
| Item One | Item Two      |
| Item One | Item Two     |
| Item One | Item Two     |


## Bilan des Tests

Les tests effectués ont permis de démontrer et valider le bon fonctionnement de l'infrastructure selon les demandes du client.

\newpage

# Conclusion

En conclusion, le projet a été extrêmement enrichissant au point de vue des connaissances. Il a permis un bon travail de groupe et une mise en pratique de ce que nous avions appris dans le module "Cisco".

\newpage

# Références

## Bibliographie

Aucun ouvrage n'a été consulté durant la rédaction du présent document.

## Webographie

Vous trouverez la liste des sites web consultés durant la rédaction du document.

http://www.foss-cloud.org/en/wiki/Installation_Multi-Node

http://www.foss-cloud.org/en/wiki/Kernel_Modules_-_Load,_Unload,_Configure

http://www.foss-cloud.org/en/wiki/FOSS-Cloud_Broker_Daemon

http://www.foss-cloud.org/en/wiki/Virtual_Desktop_Infrastructure

https://sourceforge.net/p/foss-cloud/bugs/164/

https://github.com/oliverguenther/foss-cloud-perl-installer


\newpage

# Annexes

Ment.
